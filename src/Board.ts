import GameObject from "./BoardObjects/GameObject";
import Player from "./BoardObjects/Player";
import GroundBlock from "./BoardObjects/GroundBlock";
import Point from "./Primitives/Point";
import DecorationObject from "./BoardObjects/Decorations/DecorationObject";

export default class Board {
    public offset = new Point(0, 0);
    protected _player: Player;
    protected _objects: GameObject[] = [];
    protected _decorations: DecorationObject[] = [];

    public width: number;
    public height: number;

    protected _context: CanvasRenderingContext2D;

    constructor() {
        let canvas = document.createElement('canvas');

        canvas.id = 'Board';
        canvas.width = this.width = document.body.clientWidth;
        canvas.height = this.height = window.innerHeight - 20;

        let body = document.querySelector('body');
        body.appendChild(canvas);

        this._context = canvas.getContext('2d');
    }

    get virtualWidth() {
        return this.width + this.offset.x;
    }

    get virtualHeight() {
        return this.height - this.offset.y;
    }



    private clear() {
        this.context.clearRect(0, 0, this.width, this.height);
    }

    public render(): void {
        this.clear();

        //todo: needs refactoring
        this._decorations.forEach((obj) => {
            obj.getRenderable().render(this);
        });

        this._objects.forEach((obj) => {
            obj.getRenderable().render(this);
        });

    }

    public addPlayer(player: Player) {
        this._player = player;
        this._objects.push(player);
    }

    public getPlayer(): Player {
        return this._player;
    }

    public addObjects(objects: GameObject[]) {
        this._objects = this._objects.concat(objects);
    }

    public addDecorationObjects(objects: DecorationObject[]) {
        this._decorations = this._decorations.concat(objects);
    }

    public getObjects(): GameObject[] {
        return this._objects.filter((obj) => {
            return !(obj instanceof GroundBlock);
        });
    }

    public getGroundObjects(): GameObject[] {
        return this._objects.filter((obj) => {
            return obj instanceof GroundBlock;
        });
    }

    get context() {
        return this._context;
    }
}