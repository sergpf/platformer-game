import DecorationObject from "./DecorationObject";
import Renderable from "../../Renderable";
import BackgroundRenderable from "../../Renders/Decorations/BackgroundRenderable";

export default class BackgroundDecoration extends DecorationObject{
    execute(): void {
    }

    getRenderable(): Renderable {
        return new BackgroundRenderable();
    }

}