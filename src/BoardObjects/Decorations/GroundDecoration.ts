import DecorationObject from "./DecorationObject";
import Renderable from "../../Renderable";
import GroundRenderable from "../../Renders/Decorations/GroundRenderable";
import Board from "../../Board";
import Point from "../../Primitives/Point";

declare var board: Board;

export default class GroundDecoration extends DecorationObject{
    private groundDots: Point[] = GroundDecoration.generateGroundDots(board);

    private static generateGroundDots(board: Board): Point[] {
        let result: Point[] = [];

        for (let i = 0; i < 1000; i++) {
            result.push(new Point(Math.random() * board.width, 500 + Math.random() * 500));
        }

        return result;
    }

    getRenderable(): Renderable {
        return new GroundRenderable(this.groundDots);
    }

    execute(): void {
    }

}