import Point from "../Primitives/Point";
import Primitive from "../Primitives/Primitive";
import Board from "../Board";
import BoardObject from "./BoardObject";

declare var board: Board;

export default abstract class GameObject extends BoardObject{
    public speed = 0;
    public point: Point;
    protected _primitive: Primitive;

    constructor(point: Point) {
        super();
        this.point = point;
    }

    getPrimitive() {
        return this._primitive;
    }

    public onGround(): boolean {
        let groundObjects = board.getGroundObjects();

        for(let obj of groundObjects) {
            //find object below
            if (this._primitive.isAbove(obj.getPrimitive())) {
                return true;
            }
        }

        return false;
    }

    public fall(speed: number): void {
        this.point.y += speed;
    }
}