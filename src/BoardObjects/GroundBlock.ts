import GameObject from "./GameObject";
import Renderable from "../Renderable";
import Rectangle from "../Primitives/Rectangle";
import GroundBlockRender from "../Renders/GroundBlockRender";
import Point from "../Primitives/Point";

export default class GroundBlock extends GameObject {
    readonly _height = 40;

    constructor(point: Point, width: number) {
        super(point);
        this._primitive = new Rectangle(this.point, width, this._height);
    }

    getRenderable(): Renderable {
        return new GroundBlockRender(this._primitive);
    }

    execute(): void {
    }
}