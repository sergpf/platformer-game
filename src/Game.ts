import Map from "./Levels/Map";
import Board from "./Board";
import Player from "./BoardObjects/Player";
import Point from "./Primitives/Point";
import Gravity from "./Gravity";
import Camera from "./Camera";

export default class Game {
    _over: boolean = false;
    _board: Board;

    constructor() {
        this._board = new Board();
        (window as any).board = this._board;
    }

    public loadMap(map: Map) {
        let decorations = map.getDecorations();
        let mapObjects = map.getObjects();

        this._board.addDecorationObjects(decorations);
        this._board.addObjects(mapObjects);
    }

    public addPlayer() {
        let player = new Player(new Point(100, 400));
        this._board.addPlayer(player);
    }

    public start() {
        let gravity = new Gravity(this._board);
        let player = this._board.getPlayer();
        let camera = new Camera(player);
        (window as any).gravity = gravity;
        (window as any).camera = camera;

        document.addEventListener('keydown', (e) => {
            switch (e.key) {
                case 'ArrowUp':
                    player.jump();
                    break;
                case 'ArrowLeft':
                    player.moveLeft();
                    break;
                case 'ArrowRight':
                    player.moveRight();
                    break;
                default:
                    break;
            }
        });

        document.addEventListener('keyup', (e) => {
            switch (e.key) {
                case 'ArrowUp':
                    player.stopJump();
                    break;
                case 'ArrowLeft':
                    player.stopMoveLeft();
                    break;
                case 'ArrowRight':
                    player.stopMoveRight();
                    break;
                default:
                    break;
            }
        });

        let f = () => {
            if (player.point.y > this._board.height) {
                this._over = true;
            }

            gravity.work();
            camera.work();

            for(let obj of this._board.getObjects()) {
                obj.execute();
            }
            this._board.render();
            camera.render(this._board);

            if (!this._over) {
                requestAnimationFrame(f);
            } else {
                console.log('Game Over!');
            }
        };
        f();
    }
}