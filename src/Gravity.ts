import Board from "./Board";

export default class Gravity {
    readonly forceValue = 5;
    private _board: Board;

    constructor(board: Board) {
        this._board = board;
    }

    public work(): void {
        let gameObjects = this._board.getObjects();

        for (let gameObject of gameObjects) {
            if (!gameObject.onGround()) {
                gameObject.fall(this.forceValue);
            }
        }
    }
}