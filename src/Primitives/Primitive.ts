import Point from "./Point";

export default abstract class Primitive {
    public point: Point;
    public width: number;
    public height: number;

    constructor(point: Point, width: number, height: number) {
        this.point = point;
        this.width = width;
        this.height = height;
    }

    abstract center(): Point;

    abstract isAbove(primitive: Primitive): boolean;
}