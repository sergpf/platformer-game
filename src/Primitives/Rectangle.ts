import Primitive from "./Primitive";
import Point from "./Point";

const allowedE = 10;

export default class Rectangle extends Primitive{

    isAbove(primitive: Primitive): boolean {
        let rightTopCornerX = this.point.x + this.width;
        let leftBottomCornerY = this.height + this.point.y;

        let primitiveRightTopCornerX = primitive.point.x + primitive.width;

        if ((this.point.x >= primitive.point.x || (rightTopCornerX > primitive.point.x))
            && this.point.x < primitiveRightTopCornerX) {
            return leftBottomCornerY >= primitive.point.y && leftBottomCornerY < primitive.point.y + allowedE;
        }
        return false;
    }

    center(): Point {
        let x =  this.point.x + this.width / 2;
        let y =  this.point.y + this.height / 2;

        return new Point(x, y);
    }

}