import Board from "./Board";

export default interface Renderable {
    render(board: Board): void;
}