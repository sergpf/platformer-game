import Renderable from "../../Renderable";
import Board from "../../Board";

export default class BackgroundRenderable implements Renderable{
    render(board: Board): void {
        board.context.fillStyle="#AFC6FF";
        board.context.fillRect(0, 0, board.width, board.height);
    }

}