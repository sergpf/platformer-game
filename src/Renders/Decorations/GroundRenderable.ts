import Renderable from "../../Renderable";
import Board from "../../Board";
import Point from "../../Primitives/Point";

export default class GroundRenderable implements Renderable{

    constructor(private groundDots: Point[]) {}

    render(board: Board): void {
        board.context.fillStyle="#5b2714";
        board.context.fillRect(0, 500 + board.offset.y, board.width, board.height);
        board.context.fillStyle="#913E20";

        for(let obj of this.groundDots){
            board.context.fillRect(obj.x, obj.y + board.offset.y, 2,2);
        }
    }
}