import RectangleRender from "./RectangleRender";
import Board from "../Board";

export default class PlayerRender extends RectangleRender{
    render(board: Board): void {
        board.context.fillStyle = '#3887ff';
        super.render(board);
    }

}