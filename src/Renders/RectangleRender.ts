import Renderable from "../Renderable";
import Board from "../Board";
import Rectangle from "../Primitives/Rectangle";

export default class RectangleRender implements Renderable{
    private _rectangle: Rectangle;

    constructor(rectangle: Rectangle) {
        this._rectangle = rectangle;
    }

    render(board: Board): void {
        board.context.fillRect(this._rectangle.point.x - board.offset.x, this._rectangle.point.y + board.offset.y, this._rectangle.width, this._rectangle.height);
    }

}