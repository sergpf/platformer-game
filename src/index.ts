import Level1 from "./Levels/Level1";
import Game from "./Game";


const game = new Game();

game.addPlayer();
game.loadMap(new Level1());
game.start();